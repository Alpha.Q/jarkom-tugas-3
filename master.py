#!/usr/bin/env python3

from socket import *
from ast import literal_eval
import subprocess
import selectors
import types
from queue import Queue


sel = selectors.DefaultSelector()

## Get the server ready to listen
serverPort = 1029
serverSocket = socket(AF_INET, SOCK_STREAM)

serverSocket.bind(('', serverPort))
serverSocket.listen(1)

print('The Master Node is ready to receive job from client')

serverSocket.setblocking(False)
sel.register(serverSocket, selectors.EVENT_READ, data=None)

# wanna implement queue with list
worker_queue = Queue()
index_worker = 0
worker_queue.put(('54.91.91.17', 1029))
worker_queue.put(('52.87.159.80', 1029))
worker_queue.put(('34.224.101.206', 1029))
serverName = 1
serverPort = 1

def accept_wrapper(sock):
    conn, addr = sock.accept()  # Should be ready to read

    print('accepted connection from', addr)
    conn.setblocking(False)
    data = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')

    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    sel.register(conn, events, data=data)

def service_connection(key, mask):
    global serverName, serverPort, worker_queue
    sock = key.fileobj
    data = key.data
    if mask & selectors.EVENT_READ:
        recv_data = sock.recv(1024)  # Should be ready to read
        args_string = recv_data.decode('utf-8')

        if args_string:
            serverName, serverPort = worker_queue.get() 

            # Connect to server
            clientSocket = socket(AF_INET, SOCK_STREAM)

            clientSocket.connect((serverName, serverPort))

            # Send arguments
            clientSocket.send(str(args_string).encode('utf-8'))
            # Get response from server
            modifiedSentence = clientSocket.recv(2048)
            # Print response

            data.outb = modifiedSentence.decode('utf-8')

            clientSocket.close()
        else:
            print('closing connection to', data.addr)
            worker_queue.put((serverName, serverPort))
            sel.unregister(sock)
            sock.close()

    if mask & selectors.EVENT_WRITE:
        if data.outb:
            print('echoing response', 'to', data.addr)
            data.outb = str(data.outb).encode('utf-8')
            sent = sock.send(data.outb)  # Should be ready to write
            data.outb = data.outb[sent:]

# Accept every client that connects to server
while 1:
    events = sel.select(timeout=None)
    for key, mask in events:
        if key.data is None:
            accept_wrapper(key.fileobj)
        else:
            service_connection(key, mask)



    # send response to client
#    connectionSocket.send(response)
#    connectionSocket.close()
