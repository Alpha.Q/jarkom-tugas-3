# Jarkom Tugas 3

Mengimplementasikan Socket Programming untuk menerapkan Job Management System dengan paradigma komputasi Master-Worker

## Flow of The Program
client.py dapat dijalankan oleh siapa saja dan mengirimkan job kepada master node dengan identifikasi IP dan port master node pada flag pemanggilan client.py. Nantinya, master node akan bertindak sebagai master sekaligus sebagai portal yang akan melakukan job management dan menentukan worker mana yang akan menyelesaikan job yang diminta oleh client tersebut. Registrasi worker node dapat dilakukan pada bagian program master dengan menambahkan tuple (IP, port) pada queue bernama worker_queue.

## Type of Job that client can request
-greet name: Job that will greet you
<br/>-fibonacci: Show first 35 fibonacci number
<br/>-toBinary decNumber: Convert decimal number (decNumber) to binary
<br/>-ping destinationURL. Show the average of internet connection to destination URL after one minute ping the destination URL 

## Contributor
1. I Made Krisna Dwitama
2. Muhammad Salman Al Farisi
3. Riri Edwina Renaganis