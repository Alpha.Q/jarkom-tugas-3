#!/usr/bin/env python3

from socket import *
from ast import literal_eval
import subprocess
import selectors
import types

def get_output(command):
    out = subprocess.Popen(command.split(" "), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout,stderr = out.communicate()
    
    stdout = stdout.decode('utf-8').split("\n")
    return stdout

def dec_to_binary(number):
    number = int(number)
    result = ''

    while number > 0:
        result = str(number % 2) + result 
        number = number // 2

    return result

def fibo(index):
    if(index <= 1):
        return 1
    return fibo(index - 1) + fibo(index - 2)

def check_connectivity(kemana):
    response_temp = ["CONNECTIVITY"]
    count = 30
    interval = 2
    command = "ping " + str(kemana) + " -c " + str(count) + " -i " + str(interval)
    stdout = get_output(command)
    
    result = ""
    if "unreachable" in stdout[0]:
        result += "Cant connect to the website"
    else:
        ratarata = 0
        for kata in stdout:
            i = kata.split()
            for j in i:
                if j[:5] == 'time=':
                    now = float(j[5:])
                    ratarata = ratarata + now
        ratarata /= count
        result += "Can connect to the website with average ping: " + str(ratarata)

    return result

sel = selectors.DefaultSelector()

## Get the server ready to listen
serverPort = 1029
serverSocket = socket(AF_INET, SOCK_STREAM)

serverSocket.bind(('', serverPort))
serverSocket.listen(1)

print('The worker is initiated. Currently free to doing a job')

serverSocket.setblocking(False)
sel.register(serverSocket, selectors.EVENT_READ, data=None)

def accept_wrapper(sock):
    conn, addr = sock.accept()  # Should be ready to read

    print('accepted connection from', addr)
    conn.setblocking(False)
    data = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')

    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    sel.register(conn, events, data=data)

def service_connection(key, mask):
    sock = key.fileobj
    data = key.data
    if mask & selectors.EVENT_READ:
        recv_data = sock.recv(1024)  # Should be ready to read
        args_string = recv_data.decode('utf-8')
        if args_string:
            args = literal_eval(args_string)
            if args:
                arg = args[0]

            data.outb = ""

            if arg == '-greet':
                greetings = "Halo " + ' '.join(args[1:])
                data.outb = greetings
            elif arg == '--help': # help
                data.outb += "Usage:\n client.py -s masterIP -p masterPort options\n"
                data.outb += "\nOptions:\n"
                data.outb += " --help,\t\t Show help message\n"
                data.outb += " -s serverName,\t\t Set master ip\n"
                data.outb += " -p serverPort,\t\t Set master port\n"
                data.outb += " -greet name,\t\t will greet you\n"
                data.outb += " -fibonacci,\t\t Show first 35 fibonacci number\n"
                data.outb += " -toBinary decNumber,\t\t Convert decimal number (decNumber) to binary\n"
                data.outb += " -ping destinationURL,\t\t Show the average of internet connection to destination URL\n"
            elif arg == '-toBinary':
                data.outb = dec_to_binary(args[1])
            elif arg == '-fibonacci':
                maks = 35
                data.outb = "Berikut adalah " + str(maks) + " Fibonacci pertama :\n"
                for i in range(0, maks):
                    data.outb += str(fibo(i)) + " "
            elif arg == '-ping':
                data.outb = check_connectivity(' '.join(args[1:]))
            else:
                data.outb += arg + " argument doesn't exist in server. Use --help to know the feature\n"
        else:
            print('closing connection to', data.addr)
            sel.unregister(sock)
            sock.close()
    
    if mask & selectors.EVENT_WRITE:
        if data.outb:
            print('echoing response', 'to', data.addr)
            data.outb = str(data.outb).encode('utf-8')
            sent = sock.send(data.outb)  # Should be ready to write
            data.outb = data.outb[sent:]

# Accept every client that connects to server
while 1:
    events = sel.select(timeout=None)
    for key, mask in events:
        if key.data is None:
            accept_wrapper(key.fileobj)
        else:
            service_connection(key, mask)

    
    
    # send response to client
#    connectionSocket.send(response)
#    connectionSocket.close()