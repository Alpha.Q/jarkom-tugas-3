#!/usr/bin/env python3

from socket import *
import sys

args = sys.argv[1:] # Get terminal arguments

# Default server
serverName = '34.207.194.124'
serverPort = 1029

if not args:
    print("Use --help flag to know the feature")
    exit()

if '-s' not in args or '-p' not in args:
    print("Please declare the master IP in -s flag and the master port in -p")
    exit()

# given serverName
if '-s' in args :
    serverName = args[args.index('-s') + 1]
    args.remove('-s')
    args.remove(serverName)

# given serverPort
if '-p' in args :
    serverPort = int(args[args.index('-p') + 1])
    args.remove('-p')
    args.remove(str(serverPort))

# Connect to server
clientSocket = socket(AF_INET, SOCK_STREAM)

clientSocket.connect((serverName, serverPort))

# Send arguments
clientSocket.send(str(args).encode('utf-8'))
# Get response from server
modifiedSentence = clientSocket.recv(2048)
# Print response
print (modifiedSentence.decode('utf-8'))

clientSocket.close()